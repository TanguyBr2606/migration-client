<?php

require_once '../config.php';

// Affiche que le script est disponible qu'en ligne de commande
if (php_sapi_name() !== 'cli') {
    echo 'Script uniquement disponible en ligne de commande';
    die;
}
// Vérifie si le fichier existe ou pas dans le répertoire
if (!$argv[1] || !file_exists($argv[1])) {
    echo 'le fichier n\'existe pas';
    die;
}

// Récupération du nom du fichier SQL depuis la ligne de commande
$fileName = $argv[1];

// Récupération du nom du fichier sans l'extension .sql
$nameDB = strstr($fileName, '.', true);

// Connexion DB
$pdo = new PDO("mysql:host=" . DB_HOST, DB_USER, DB_PASSWORD);

// Vérification si la DB existe déjà
if (!$pdo->exec("CREATE DATABASE IF NOT EXISTS $nameDB")) {
    echo "La base de Données $nameDB existe déjà";
    die;
}

// Lecture du fichier SQL
$sql = file_get_contents($fileName);

$pdo = new PDO("mysql:host=" . DB_HOST . ";dbname=" . $nameDB, DB_USER, DB_PASSWORD);

$pdo->exec($sql);

echo 'Migration de la base de données ' . $nameDB . ' réussie.';
// Fermeture de la DB
$pdo = null;
