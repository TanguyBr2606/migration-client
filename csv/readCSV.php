<?php

if (!empty($_SERVER['argv'])) {
    $argv = $_SERVER['argv'];
} else {
    echo 'Script uniquement disponible en ligne de commande';
    die;
}

if (!$argv[1] || !file_exists($argv[1])) {
    echo 'le fichier n\'existe pas !';
    die;
}
$array = [];
$file  = $argv[1];
// Affectation d'une variable qui utilise la fonction native fopen pour ouvrir le fichier CSV
// fopen : https://www.php.net/manual/fr/function.fopen.php
$source = fopen($file, "r");
// On compare le résultat de $file, s'il n'est pas égale à false (inégalité stricte)
if ($source !== false) {
    // Affectation d'une variable qui utilise la fonction native fgetcsv qui analyse les lignes du fichier CSV
    // qui compare le résultat de la variable $data, s'il n'est pas égale à false (inégalité stricte)
    // fgetCSV : https://www.php.net/manual/fr/function.fgetcsv.php
    while (($data = fgetcsv($source, 300)) !== false) {
        $object       = new stdClass();
        $object->id   = $data[0];
        $object->name = $data[1];
        $object->code = $data[2];
        $array[]      = $object;
    }
    // Utilisation de la fonction fclose pour fermer la ressource du fichier.
    // https://www.php.net/manual/fr/function.fclose.php
    fclose($source);
}
var_dump($array);


// file_get_contents : query PDO
